# Dummy makefile for creating debian package
ARCH = arm
CROSS_COMPILE ?= arm-linux-gnueabihf-

MAKEFLAGS += --no-print-directory

.PHONY: all build clean distclean

build:

all:

clean:

distclean: clean

