#!/bin/bash

mkdir builds
cd builds

COMPONENT=framework_components
COMPONENT_SHORT=fc
VERSION=3_31_00_02

#RELEASE=${COMPONENT}_${VERSION}_eng
RELEASE=${COMPONENT}_${VERSION}
COMPONENT_PPA=${COMPONENT_SHORT}ppa

# public link
#wget --no-proxy http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/$COMPONENT_SHORT/$VERSION/exports/${RELEASE},lite.tar.gz

# internal link
wget --no-proxy http://www.sanb.design.ti.com/tisb_releases/Framework_Components/${VERSION}/exports/${RELEASE},lite.tar.gz

tar -xzvf $RELEASE,lite.tar.gz

git clone git://git.ti.com/mfp/${COMPONENT_PPA}.git

cd $RELEASE

cp -r ../$COMPONENT_PPA/debian .
#cp ../$COMPONENT_PPA/Makefile .

cd ..

tar -czvf ${RELEASE}_ppa,lite.tar.gz $RELEASE

